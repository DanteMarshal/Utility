# pSay
pSay (pico2wave say) is a wrapper script for pico2wave on ubuntu which tries to implement a selection / clipboard based TTS for ubuntu. The scripts can be used as a launcher on panels to make it possible to have a "TTS on Click" system on ubuntu.

## Requirements
For `psay` and `psilent` scripts, Only `pico2wave` is required which can be installed using the following command on ubuntu  
``` sudo apt-get install libttspico0 libttspico-utils libttspico-data ```  

If you're going to use `psayclip` script, you're also going to need `xclip` installed  
``` sudo apt-get install xclip ```

# Scripts
Below I describe what each script does and how to use them !

### psay
This is the main script, You give it text and get sound played.  
`usage : psay "Text !"`

### psilent
Call this script to silence what was being played. It's simply recalling psay with an empty text to overwrite the temporary buffer .wav file.  
`usage : psilent`

### psayclip
Call this script to turn whatever text is in your clipboard (or selection) to sound. This script uses `xclip` to get the contents of your clipboard or selection. You can select a text (or copy one) then call this command to speak it.  
`usage : psayclip`
