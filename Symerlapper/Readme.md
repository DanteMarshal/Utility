# Symerlapper (Symlink Overlapper)
A Python script to create a virtually overlapping filesystem, Merging two similar directory structures using Symlinks.
It looks somehow similar to UnionFS, AUFS and OverlayFS, Except that it doesn't Mount anything.
## How it works
This script requires 3 arguments, an Upper Directory, a Lower Directory, and a Target Directory.
Upper Directory (Upper source) is the Source directory with higher priority, the Lower Directory (Lower source) is the Source directory with lower priority and
the Target Directory is where Source directories are Merged by links.
The script creates directories and symbolic links - with Absolute paths - to merge the two folders as a whole. The Merge happens following these general rules :

- If any link to any directory exists in a source, it is treated like a file.
- All files are linked from sources to the target. In each level, files are linked first, then directories.
- If a directory or a file is unique in One of sources (Nothing with same relative path exists in the other source), It is simply linked to the Target.
- If two files have the same relative path from their source, The file from Upper source is linked to Target, and Lower source is ignored.
- If a file and a directory have the same relative path from their source, The file is prioritized.
- If two directories have the same relative path from their source, A new directory is created in Target. This new directory is recursively merged from sources until the Upper and Lower source are merged into Target.
- If there are any old files in Target, that don't exist in any of the sources; It can be overwritten using "-o overwrite" option, or files can be backed up using "-o backup". If no option is given, no link is created when a file already exists.
- If there are any old directories in Target, that don't exist in any of the sources; It can be backed up using "-o backup" option. The "-o overwrite" doesn't work with directories. If no option is given, no link is created when a directory already exists.

## Usage, Requirements and License
Simply run the script using Python 2.7 or higher, giving it the required arguments.  
This script is useful when merging two directories from two disks or drives is required. This way one can keep their disk contents separate, and also have the virtually merged filesystem they want.
Though, it can't help when creating a new file or directory inside a directory that has the same path in both sources, You need to manually create the file / directory inside Upper or Lower source (Whichever you need), and then re-run the script or manually link the new directory to your Target.

This script is under "MIT License"