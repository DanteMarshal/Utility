import errno
import os
import sys

overwrite = 'N'


def make_dir_if_not_exists(dir_to_make, perms):
    try:
        os.makedirs(dir_to_make, mode=perms)
    except OSError as E:
        if E.errno != errno.EEXIST:
            raise


def link_dirs(src, trg, drs, dfud):
    for drn in drs:
        lnk_src = os.path.join(src, drn)
        lnk_dst = os.path.join(trg, drn)
        if os.path.isdir(lnk_dst):
            if drn in dfud:
                print '"' + lnk_dst + '" is overlapped by upper directory"'
                return
            elif overwrite == 'B':
                print 'Target directory "' + lnk_dst + '" already exists.\nNow renamed to "' + lnk_dst + '.backup"'
                try:
                    os.rename(lnk_dst, lnk_dst + ".backup")
                except OSError:
                    print 'Failed to backup "' + lnk_dst + '"'
                    return
            else:
                print 'Target directory "' + lnk_dst + '" already exists.'
                return
        try:
            os.symlink(lnk_src, lnk_dst)
        except OSError:
            print 'Failed to create symlink : "' + lnk_dst + '"'


def link_files(src, trg, ffud):
    # ffud : Files from The Upper directory (Empty if src is Upper)
    sfs = [fn for fn in os.listdir(src) if os.path.isfile(os.path.join(src, fn)) or os.path.islink(os.path.join(src, fn))]
    for sfn in sfs:
        lnk_src = os.path.join(src, sfn)
        lnk_dst = os.path.join(trg, sfn)
        try:
            os.symlink(lnk_src, lnk_dst)
        except OSError, E:
            if E.errno == errno.EEXIST:
                if sfn in ffud:
                    print '"' + lnk_dst + '" is overlapped by upper directory"'
                elif overwrite == 'O':
                    print 'Overwriting file "' + sfn + '" in "' + trg + '"'
                    try:
                        os.remove(lnk_dst)
                        try:
                            os.symlink(lnk_src, lnk_dst)
                        except OSError:
                            print 'Failed to overwrite symlink : "' + lnk_dst + '"'
                    except OSError:
                        print 'Failed to remove "' + lnk_dst + '"'
                elif overwrite == 'B':
                    print 'Backing up file "' + sfn + '" in "' + trg + '"'
                    try:
                        os.rename(lnk_dst, lnk_dst + '.backup')
                        try:
                            os.symlink(lnk_src, lnk_dst)
                        except OSError:
                            print 'Failed to create symlink : "' + lnk_dst + '"'
                    except OSError:
                        print 'Failed to rename "' + lnk_dst + '"'
                else:
                    print 'Target file "' + lnk_dst + '" already exists"'
            else:
                print 'Failed to create symlink : "' + lnk_dst + '"'
    return sfs


def get_commons(dr1, dr2):
    d1 = [drn for drn in os.listdir(dr1)
          if os.path.isdir(os.path.join(dr1, drn)) and not os.path.islink(os.path.join(dr1, drn))]
    d2 = [drn for drn in os.listdir(dr2)
          if os.path.isdir(os.path.join(dr2, drn)) and not os.path.islink(os.path.join(dr2, drn))]
    d1 = set(d1)
    d2 = set(d2)
    cm = d1 & d2
    d1 = set(d1) - cm
    d2 = set(d2) - cm
    return list(cm), list(d1), list(d2)


def overlap(dir1, dir2, trg):
    make_dir_if_not_exists(trg, os.lstat(dir1).st_mode)
    fs1 = link_files(dir1, trg, [])
    link_files(dir2, trg, fs1)
    cm, d1, d2 = get_commons(dir1, dir2)
    link_dirs(dir1, trg, d1, [])
    link_dirs(dir2, trg, d2, list(set(d1) | set(fs1)))
    for cm_dir in cm:
        new_trg = os.path.join(trg, cm_dir)
        new_dr1 = os.path.join(dir1, cm_dir)
        new_dr2 = os.path.join(dir2, cm_dir)
        make_dir_if_not_exists(new_trg, os.lstat(new_dr1).st_mode)
        overlap(new_dr1, new_dr2, new_trg)


if len(sys.argv) < 4:
    print "Usage : "
    print "\tpython Symerlapper.py [UPPER_DIR] [LOWER_DIR] [TARGET_DIR]"
    print "\tpython Symerlapper.py [UPPER_DIR] [LOWER_DIR] [TARGET_DIR] [-o {overwrite | backup}]"
    print "if '-o overwrite' is mentioned, Old files are overwritten !"
if len(sys.argv) > 5 and sys.argv[4] == "-o":
    if sys.argv[5] == 'overwrite':
        overwrite = 'O'
    elif sys.argv[5] == 'backup':
        overwrite = 'B'
overlap(sys.argv[1], sys.argv[2], sys.argv[3])
