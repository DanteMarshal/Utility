import os
import subprocess
import sys

LType = "-s"
Target = None


def parse_args():
    global Target
    global LType
    args = sys.argv
    args.remove(args[0])
    if len(args) < 2:
        print "No argument specified."
        exit(-1)
    else:
        for A in args:
            if A.startswith('-'):
                if A.lower() == "-r":
                    LType = "-sr"
                elif A.lower() == "-s":
                    LType = "-s"
                elif A.lower() == "-?":
                	print "Usage :"
                	print "PasteAsLink.py [LinkType (Optional)] [TargetDir]"
                	print "Link Type :"
                	print "\t-s = Absolute Symbolic Link (Default Behavior)"
                	print "\t-r = Relative Symbolic Link"
                else:
                    print "Undefined option Ignored : " + A
            elif Target is None:
                Target = A
            else:
                print "Extra Option Ignored : " + A
    if Target is None:
        print "No Target Directory specified"
        exit(-1)


def get_files():
    out = ''
    try:
        out = subprocess.check_output("xclip -o -selection clipboard -t x-special/gnome-copied-files".split())
    except:
        print "Failed to get clipboard contents."
        exit(-1)
    out = out.split()
    if len(out) < 2:
        print "Either no file is copied, or there's another problem with Clipboard."
    if out[0] != "copy":
        print "Note that Cut doesn't work, you need to Copy the files"
        exit(1)
    Files = []
    for i in range(1, len(out)):
        Files.append(out[i])
    return Files


def make_link(path, name):
    if LType == "" and os.path.isdir(path):
        print "Cannot hard link Directories. Ignoring : " + path
        return
    full_name = '.'.join(name)
    final_path = Target + "/" + full_name
    last_num = 0
    while os.path.exists(final_path):
        name[0] = name[0].rsplit("(Link", 1)
        name[0] = name[0][0]
        last_num += 1
        name[0] += " (Link " + str(last_num) + ")"
        full_name = '.'.join(name)
        final_path = Target + "/" + full_name
    cmd = ["ln", LType, "-T", path, final_path]
    subprocess.Popen(cmd, stdout=subprocess.PIPE)

def main(files):
    for i in range(0, len(files)):
        files[i] = files[i].replace("file://", "").replace("%20", " ")
        name = files[i].split("/")
        name = name[len(name) - 1].rsplit(".", 1)
        if len(name) > 1 and name[0] == "":
            name[0] = "." + name[1]
            del name[1]
        make_link(files[i], name)


parse_args()
main(get_files())
