# Paste as Link (Python Script for Linux)
Here's a python script using "ln" linux command and "xclip" to paste copied files as links.

# Requirements
- Python 2.7 or higher
- A Linux OS (Created and Tested on XUbuntu 18.04)
- xclip (Mostly available in official repositories)

# How to use
After copying your files and directories, Pass the directory you want to create your links in (Your Target directory) as argument to this python script, And here are your links.

# Integration with File Managers
Personally, I created this script for my File manager (Thunar) on XUbuntu 18.04, But it can be used for other purposes too.
It can also be Integrated into other file managers which do not support easy creation of links as a GUI or Hotkey command.

## File manager : Thunar
In order to integrate this script into your Thunar file manager ...

1. Download the script from this repository, and put it somewhere your know (Home or Thunar Config directory is suggested)
2. Create a Custom Action in Thunar (Edit -> Configure custom actions ... -> Add a new custom action ... (Green Plus Sign))
3. In "Basic" tab, enter a Name (i.e. "Paste as Symlink") and Description (Or just leave it empty) for your custom action.
4. For command enter : `python 'PATH/TO/SCRIPT.py' -s %f`, Replacing `PATH/TO/SCRIPT.py` with where you chose to keep the script.
5. Choose an Icon for the action if you like (Or just leave it alone without an icon !)
6. In "Appearance Conditions" tab, Tick all file types and just leave the * for file pattern.
7. Done, You now have the script integrated into your right click menu in directories.

(Optional) You can use -r instead of -s before %f, so that it uses relative address instead of absolute for the symlink.
(Optional) You can also setup a Hotkey for this action, So that you can paste things as links even easier.

# Features
- Pastes multiple files and directories at the same time
- Uses "(Link #)" suffix when a file exists
- Using "-r" option, Creates Relative Symlinks instead of Symlinks with absolute paths

### Maybe-I'll-add-later Features
- Create Hardlinks too (using a second argument)

# License
MIT, Just take it and use it.
Though thsi script so small, But contributions and suggestions are always welcome !
